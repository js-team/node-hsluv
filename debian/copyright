Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: hsluv
Upstream-Contact: https://github.com/hsluv/hsluv/issues
Source: http://www.hsluv.org
Comment:
 This source package contains the subdirectory "./javascript" from
 https://github.com/hsluv/hsluv -- a repository without any tagging.
 To have a useful d/watch we package the tarball from the npm registry.
 The hsluv.js distributed by that tarball is compiled by haxe and thus
 we remove it from the upstream tarball and rebuild it from the haxe
 sources in debian/missing-sources.
Files-Excluded: hsluv.js

Files: *
Copyright: 2012-2021, Alexei Boronine <alexei@boronine.com>
License: Expat

Files: debian/*
Copyright: 2021, Johannes Schauer Marin Rodrigues <josch@debian.org>
License: Expat

Files: debian/missing-sources/*
Copyright: 2012-2021, Alexei Boronine <alexei@boronine.com>
License: Expat

License: Expat
 Permission is hereby granted, free of charge, to any person
 obtaining a copy of this software and associated documentation files
 (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge,
 publish, distribute, sublicense, and/or sell copies of the Software,
 and to permit persons to whom the Software is furnished to do so,
 subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be
 included in all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
